import pymongo

class DbManager:
    __host = "localhost"
    __db = "bioinformatica"
    __collClasses = "classes"
    __collSequences = "sequences"

    @staticmethod
    def __getClient():
        return pymongo.MongoClient(host=DbManager.__host)

    @staticmethod
    def __getDb():
        return DbManager.__getClient()[DbManager.__db]

    @staticmethod
    def __getCollection(name):
        return DbManager.__getDb()[name]

    # Collections
    @staticmethod
    def getCollectionSequences():
        return DbManager.__getCollection(DbManager.__collSequences)