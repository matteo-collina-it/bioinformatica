import os
import matplotlib.pyplot as plt
import itertools
import numpy as np
from funcsigs import signature
from sklearn.metrics import average_precision_score,precision_recall_curve
import seaborn as sns
from sklearn.metrics import roc_curve,roc_auc_score
import csv

def basePath():
    return os.path.dirname(os.path.realpath(__file__))

def getRanges():
    '''
    AE: {
        from_index: 100,
        to_index: 200,
    }
    :return: dizionario degli indici di inizio e fine di ogni etichetta
    '''
    classes_path = basePath() + "/data/GM12878.csv"

    # Get all ranges of tasks
    ranges = dict()
    last_task = ""
    id_task = 0
    with open(classes_path) as fp:
        for index,line in enumerate(fp):
            val = line.strip().upper()
            if last_task != val:
                id_task +=1
                ranges.update({val:{'from_index':index, 'id_task':id_task}})
                ranges[last_task if last_task else val]["to_index"] = index
                last_task = val

    return ranges


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.yticks(tick_marks, classes)
    plt.xticks(tick_marks, classes, rotation=45)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()



def plot_pr_curve(y_test,y_pred):
    # In matplotlib < 1.5, plt.fill_between does not have a 'step' argument
    precision, recall, thresholds = precision_recall_curve(y_test, y_pred)
    average_precision = average_precision_score(y_test, y_pred)

    step_kwargs = ({'step': 'post'}
                   if 'step' in signature(plt.fill_between).parameters
                   else {})
    plt.step(recall, precision, color='b', alpha=0.2,
             where='post')
    plt.fill_between(recall, precision, alpha=0.2, color='b', **step_kwargs)

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])
    plt.title('2-class Precision-Recall curve: Avg Precision={0:0.2f}'.format(
        average_precision))

def save_dict_as_csv(dict,filename):
    w = csv.writer(open("{0}.csv".format(filename), "w"))
    for key, val in dict.items():
        w.writerow([key, val])

def save_list_dict_as_csv(l,csv_columns,filename):
    '''
    :param l: list of dict obj
    :param csv_columns: list of names of columns
    :param filename: name of file
    :return:
    '''
    csv_file = "{0}.csv".format(filename)
    try:
        with open(csv_file, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in l:
                writer.writerow(data)
    except IOError:
        print("I/O error")


def plot_roc_curve(y_test,y_pred,label='ROC curve'):
    fpr, tpr, _ = roc_curve(y_test, y_pred)
    auc = roc_auc_score(y_test, y_pred)
    plt.plot(fpr, tpr, label="{0}, auc=".format(label) + str(auc))
    plt.legend(loc=4)
    plt.xlabel('1 - Specificity')
    plt.ylabel('Sensitivity (= Recall)')
    plt.title('ROC Curve')
