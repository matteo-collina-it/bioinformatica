# -*- coding: UTF-8 -*-
# Files: http://homes.di.unimi.it/valenti/DATA/ProgettoBioinf1819/
# Topic: A-E and I-E: these represent the active and inactive enhancer
from DbManager import DbManager
import os
import numpy as np
import keras
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from keras.preprocessing.text import Tokenizer
from keras.models import Sequential
from keras.layers import Dense, Flatten, Conv1D
import matplotlib.pyplot as plt
from utils import basePath, plot_confusion_matrix, plot_pr_curve, plot_roc_curve
import itertools
from sklearn.metrics import roc_auc_score, average_precision_score


DATA_PATH = basePath()+"/data/"
CLASSES_PATH = DATA_PATH + "GM12878.csv"
SEQUENCES_PATH = DATA_PATH + "sequences/GM12878.fa"

#ERR_FIX OMP: Error #15: Initializing libiomp5.dylib, but found libiomp5.dylib already initialized.
os.environ['KMP_DUPLICATE_LIB_OK']='True'

NUM_NUCLEOTIDES = 4
NUM_CLASSES = 2

def initData(classesTarget, drop=False):
    '''
    Inizializza i dati, prendendoli dai file e immagazzinandoli su mongodb
    '''
    if drop: DbManager.getCollectionSequences().drop()
    _count = DbManager.getCollectionSequences().estimated_document_count()
    if _count == 0:
        print("Loading sequences ...")
        initSequences(classesTarget)
        print("{0} new sequences added".format(DbManager.getCollectionSequences().estimated_document_count()))
    else:
        print("√ {0} sequences already loaded in collection".format(_count))

def initSequences(classesTarget):
    '''
    Crea la collezione in mongodb delle sequenze con associate le classi corrispondenti
    :return: count: numero di sequenze inserite
    '''

    with open(CLASSES_PATH) as clsCur, open(SEQUENCES_PATH) as seqCur:
        for i, cls in enumerate(clsCur):
            cls = cls.strip().upper()
            seqCur.readline()
            seq = seqCur.readline().strip().upper()
            if cls in classesTarget:
                if 'N' not in seq:
                    DbManager.getCollectionSequences().insert_one({'value': seq, 'class': cls})
                else:
                    print("Trovata sequenza in posizione {0} contraffatta (N)".format(i), seq)


def getOneHotEncodingOfValues(X):
    '''
    Esegue l'one hot encoding.
    Viene utilizzata la lista di python per aggiungere le one hot encoded matrixes e
    trasformata alla fine in numpy array per efficienza
    :param X: lista di sequenze
    :return: result: lista di matrici con applicazione di one hot encoding
    '''
    result = list()
    tokenizer = Tokenizer(char_level=True)
    for i in range(0,len(X)):
        x = X[i]
        tokenizer.fit_on_texts(x)
        sequence_of_int = tokenizer.texts_to_sequences(x)
        int_seq = list(itertools.chain.from_iterable(sequence_of_int))
        ohe = keras.utils.to_categorical(int_seq)
        if ohe.shape[1] > 4:
            ohe = np.delete(ohe,0,1)
        result.append(ohe)

    _ = np.asarray(result)
    return _


def getDataValues():
    '''
    :return:
    - result_sequence: lista di sequenze
    - result_label: lista di etichette
    '''
    result_sequence = list()
    result_label = list()
    for seq in DbManager.getCollectionSequences().find():
        result_sequence.append(seq["value"])
        result_label.append(seq["class"])
    return [result_sequence,result_label]


if __name__ == '__main__':
    initData(['A-E', 'A-P'])
    [X,y] = getDataValues()

    # maps string target to sequential integer
    d = dict([(yl, x + 1) for x, yl in enumerate(sorted(set(y)))])
    y = [d[x] for x in y]

    # one-hot encode target column
    X = getOneHotEncodingOfValues(X)
    y = keras.utils.to_categorical(y)
    y = np.delete(y, 0, 1)

    # Preparing Data For Training
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

    '''
    Sequential is the easiest way to build a model in Keras. 
    It allows to build a model layer by layer.
    '''
    # create model
    model = Sequential()

    # add model layers
    model.add(Conv1D(64, kernel_size=3, activation='relu', input_shape = (200, 4)))
    model.add(Conv1D(32, kernel_size=3, activation='relu'))
    model.add(Flatten()) #Flatten serves as a connection between the convolution and dense layers.
    #Dense significa che tutti i neuroni portano il loro contributo
    model.add(Dense(2, activation='sigmoid'))
    #Dense’ is the layer type we will use in for our output layer.
    #Softmax function (or multinomial logistic regression) is a generalization of sigmoid function to the case where we want to handle multiple classes (multi-class classification).

    # compile model using accuracy to measure model performance
    ''' 
    Adam is an optimization algorithm that can used instead of the classical stochastic gradient descent procedure to update network weights iterative based in training data.
    Stochastic gradient descent maintains a single learning rate (termed alpha) for all weight updates and the learning rate does not change during training.
    Adam realizes the benefits of both AdaGrad (Adaptive Gradient Algorithm) and RMSProp (Root Mean Square Propagation).
    '''
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

    # train the model
    history = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=3)
    y_pred = model.predict(X_test)
    rounded = [round(x[0]) for x in y_pred]
    y_pred1 = np.array(rounded,dtype='int64')

    # Array di predizioni basati sul test
    pred_values = model.predict(X_test)

    # Converto in binario le y test e le y predette
    y_test_bin = [v[1] for v in y_test]
    y_pred_bin = [0 if v[0]>v[1] else 1 for v in pred_values]

    print("Accuracy {0}".format(accuracy_score(y_test_bin, y_pred_bin)))

    auroc_metrics = roc_auc_score(y_test_bin, y_pred_bin)
    print("AUROC {0}".format(auroc_metrics))

    average_precision = average_precision_score(y_test_bin, y_pred_bin)
    print("auPRC {0}".format(average_precision))

    confusion_matrix = confusion_matrix(y_test_bin, y_pred_bin)
    print("Confusion matrix {0}".format(confusion_matrix))

    clsf_report = classification_report(y_test_bin, y_pred_bin, output_dict=True)
    print("report", clsf_report)

    index_figure = 0

    # Precision recall curve
    index_figure += 1
    f1 = plt.figure(index_figure)
    plot_pr_curve(y_test_bin, y_pred_bin)
    f1.savefig('Precision_Recall_Curve_{0}_vs_{1}.png'.format('A-E', 'A-P'))

    # Confusion matrix
    index_figure += 1
    f2 = plt.figure(index_figure)
    plot_confusion_matrix(confusion_matrix, ['A-E', 'A-P'])
    f2.savefig('Confusion_Matrix_{0}_vs_{1}.png'.format('A-E', 'A-P'))

    # Plot ROC Curve
    index_figure += 1
    f4 = plt.figure(index_figure)
    plot_roc_curve(y_test_bin, y_pred_bin)
    f4.savefig('ROC_Curve_{0}_vs_{1}.png'.format('A-E', 'A-P'))

