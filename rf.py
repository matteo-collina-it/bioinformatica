import pandas as pd
import numpy as np
from utils import getRanges,basePath,plot_confusion_matrix,plot_pr_curve,save_dict_as_csv,plot_roc_curve,save_list_dict_as_csv
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score, roc_auc_score, average_precision_score, recall_score, f1_score
import matplotlib.pyplot as plt
import seaborn as sns
from imblearn.over_sampling import SMOTE

if __name__ == '__main__':
    file_path = basePath()+"/data/epigenomic-data/GM12878.csv"
    classes_path = basePath() + "/data/GM12878.csv"
    print("loading file {0} ...".format(file_path))
    print("loading classes {0} ...".format(classes_path))
    dataset = pd.read_csv(file_path) #dataset.shape (223049, 102)
    index_figure = 0
    '''
    Tasks:
    A-E I-E,
    A-P I-P
    A-E, A-P
    A-X, I-X
    
    Metrics to do:
    auROC (area under the receiver operating characteristic curve):
    https://scikit-learn.org/stable/modules/generated/sklearn.metrics.roc_auc_score.html
    auPRC (area under the precision-recall curve): 
    - https://scikit-learn.org/stable/modules/generated/sklearn.metrics.average_precision_score.html
    - https://stats.stackexchange.com/questions/157012/area-under-precision-recall-curve-auc-of-pr-curve-and-average-precision-ap
    - https://sanchom.wordpress.com/tag/average-precision/
    '''
    tasks = [('A-E', 'I-E'), ('A-P','I-P'), ('A-E','A-P')]
    #tasks = [('A-E', 'I-E')]

    # Get all ranges of tasks
    ranges = getRanges()


    for range in tasks:
        # index della prima label del task
        from_index_a = ranges.get(range[0])["from_index"]
        to_index_a = ranges.get(range[0])["to_index"]

        # index della seconda label del task
        from_index_b = ranges.get(range[1])["from_index"]
        to_index_b = ranges.get(range[1])["to_index"]

        len_class_a = to_index_a-from_index_a-1
        len_class_b = to_index_b-from_index_b-1
        print("\nTASK {0} [{1},{2}] ({3} items) vs {4} [{5},{6}] ({7} items)".format(range[0],from_index_a,to_index_a,len_class_a,range[1],from_index_b,to_index_b,len_class_b))

        y = list()
        with open(classes_path) as fp:
            for line in fp:
                y.append(line.strip().upper())
        ya = np.array(y[from_index_a:to_index_a])
        yb = np.array(y[from_index_b:to_index_b])
        y = np.concatenate([ya, yb])

        n_cols = dataset.shape[1]
        # name of features
        header = dataset.columns.values
        # matrix with values (remove first column of index)
        Xa = dataset.iloc[from_index_a:to_index_a, 1:n_cols].values
        Xb = dataset.iloc[from_index_b:to_index_b, 1:n_cols].values
        X = np.concatenate([Xa,Xb])

        print("X shape: {0}, y shape: {1}".format(X.shape,y.shape))

        # Preparing Data For Training
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=12)

        # Count dataset
        index_figure += 1
        f3 = plt.figure(index_figure)
        sns.countplot(y=y_train)
        f3.savefig('Count_Classes_{0}_vs_{1}.png'.format(range[0], range[1]))

        # Balance with resampling
        # https://www.kaggle.com/rafjaa/resampling-strategies-for-imbalanced-datasets
        print("Unbalanced dataset, oversampling...")
        # Oversampling
        # https://beckernick.github.io/oversampling-modeling/
        sm = SMOTE(random_state=12, ratio=1.0)
        X_train, y_train = sm.fit_sample(X_train, y_train)


        # Feature Scaling
        sc = StandardScaler()
        X_train = sc.fit_transform(X_train)
        X_test = sc.transform(X_test)

        # Training the Algorithm
        classifier = RandomForestClassifier(n_estimators=100, random_state=0)
        classifier.fit(X_train, y_train)

        y_pred = classifier.predict(X_test)

        print("Confusion matrix:")
        cm = confusion_matrix(y_test, y_pred)
        print(cm)
        print("Classification report:")
        clsf_report = classification_report(y_test, y_pred,output_dict=True)
        print(clsf_report)
        print("Accuracy {0}".format(accuracy_score(y_test, y_pred)))

        # Save classification report
        save_dict_as_csv(clsf_report,'Classification_Report_{0}_vs_{1}'.format(range[0],range[1]))

        dict_values = dict([(y, x ) for x, y in enumerate(sorted(set(y_test)))])
        y_test_int = [dict_values[x] for x in y_test]
        y_pred_int = [dict_values[x] for x in y_pred]

        print("auROC {0}".format(roc_auc_score(y_test_int,y_pred_int)))
        average_precision = average_precision_score(y_test_int,y_pred_int)
        print("auPRC {0}".format(average_precision))


        # Get importances of features
        importances = classifier.feature_importances_
        std = np.std([tree.feature_importances_ for tree in classifier.estimators_],
                     axis=0)
        indices = np.argsort(importances)[::-1]

        # Print the feature ranking
        importances = classifier.feature_importances_
        std = np.std([tree.feature_importances_ for tree in classifier.estimators_],axis=0)
        indices = np.argsort(importances)[::-1]

        # Print the feature ranking
        print("Feature ranking:")
        impfeatures = list()
        for f_index,f in enumerate(header[1:]):
            impfeatures.append({'Name_Feature':f,'Index_Feature':indices[f_index],'Importance':importances[indices[f_index]]})
        save_list_dict_as_csv(impfeatures,['Name_Feature','Index_Feature','Importance'],'Ranking_features_{0}_vs_{1}'.format(range[0],range[1]))


        # Precision recall curve
        index_figure += 1
        f1 = plt.figure(index_figure)
        plot_pr_curve(y_test_int,y_pred_int)
        f1.savefig('Precision_Recall_Curve_{0}_vs_{1}.png'.format(range[0],range[1]))

        # Confusion matrix
        index_figure += 1
        f2 = plt.figure(index_figure)
        plot_confusion_matrix(cm,[range[0],range[1]])
        f2.savefig('Confusion_Matrix_{0}_vs_{1}.png'.format(range[0],range[1]))

        # Plot ROC Curve
        index_figure += 1
        f4 = plt.figure(index_figure)
        plot_roc_curve(y_test_int,y_pred_int)
        f4.savefig('ROC_Curve_{0}_vs_{1}.png'.format(range[0], range[1]))



        print("General Recall {0}".format(recall_score(y_test_int, y_pred_int)))
        print("General f1-misure {0}".format(f1_score(y_test_int, y_pred_int)))



