from keras import Sequential
from keras.layers import Dense
import os
from sklearn.preprocessing import StandardScaler
from utils import getRanges,basePath,plot_confusion_matrix,plot_pr_curve,plot_roc_curve
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score, roc_auc_score, average_precision_score, recall_score, f1_score
import matplotlib.pyplot as plt

os.environ['KMP_DUPLICATE_LIB_OK']='True'

if __name__ == '__main__':

    file_path = basePath() + "/data/epigenomic-data/GM12878.csv"
    classes_path = basePath() + "/data/GM12878.csv"

    dataset = pd.read_csv(file_path)  # dataset.shape (223049, 102)

    ranges = getRanges()

    from_index_a = ranges.get('A-E')["from_index"]
    to_index_a = ranges.get('A-E')["to_index"]

    from_index_b = ranges.get('A-P')["from_index"]
    to_index_b = ranges.get('A-P')["to_index"]

    # Matto a maiuscolo le etichette e rimuovo spazi
    y = list()
    with open(classes_path) as fp:
        for line in fp:
            y.append(line.strip().upper())
    ya = np.array(y[from_index_a:to_index_a])
    yb = np.array(y[from_index_b:to_index_b])
    y = np.concatenate([ya, yb])

    #One Hot Encoding delle etichette
    d = dict([(y, x + 1) for x, y in enumerate(sorted(set(y)))])
    y = [d[x] for x in y]
    y = to_categorical(y)
    y = np.delete(y, 0, 1)

    n_cols = dataset.shape[1]
    # name of features
    header = dataset.columns.values
    # matrix with values (remove first column of index)
    Xa = dataset.iloc[from_index_a:to_index_a, 1:n_cols].values
    Xb = dataset.iloc[from_index_b:to_index_b, 1:n_cols].values
    X = np.concatenate([Xa, Xb])

    #Split training test e test set
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

    # Feature Scaling
    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)

    #Creazione del Modello
    model = Sequential()
    model.add(Dense(80, input_dim=(X.shape[1]), init="uniform", activation="relu"))
    model.add(Dense(40, activation="relu"))
    model.add(Dense(20, activation="relu"))
    model.add(Dense(2, activation="sigmoid"))

    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

    model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=3)

    # Array di predizioni basati sul test
    y_pred = model.predict(X_test)

    # Converto in binario le y test e le y predette
    y_test_int = [int(v[1]) for v in y_test]
    y_pred_int = [0 if v[0]>v[1] else 1 for v in y_pred]

    print("Confusion matrix:")
    cm = confusion_matrix(y_test_int, y_pred_int)
    print(cm)
    print("Classification report:")
    clsf_report = classification_report(y_test_int, y_pred_int, output_dict=True)
    print(clsf_report)
    print("Accuracy {0}".format(accuracy_score(y_test_int, y_pred_int)))


    print("auROC {0}".format(roc_auc_score(y_test_int, y_pred_int)))
    average_precision = average_precision_score(y_test_int, y_pred_int)
    print("auPRC {0}".format(average_precision))


    index_figure = 0
    # Precision recall curve
    index_figure += 1
    f1 = plt.figure(index_figure)
    plot_pr_curve(y_test_int, y_pred_int)
    f1.savefig('Precision_Recall_Curve_{0}_vs_{1}.png'.format('A-E', 'A-P'))

    # Confusion matrix
    index_figure += 1
    f2 = plt.figure(index_figure)
    plot_confusion_matrix(cm, ['A-E', 'A-P'])
    f2.savefig('Confusion_Matrix_{0}_vs_{1}.png'.format('A-E', 'A-P'))

    # Plot ROC Curve
    index_figure += 1
    f4 = plt.figure(index_figure)
    plot_roc_curve(y_test_int, y_pred_int)
    f4.savefig('ROC_Curve_{0}_vs_{1}.png'.format('A-E', 'A-P'))

    print("General Recall {0}".format(recall_score(y_test_int, y_pred_int)))
    print("General f1-misure {0}".format(f1_score(y_test_int, y_pred_int)))