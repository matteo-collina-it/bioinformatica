import pandas as pd
import os
from utils import getRanges,basePath,plot_confusion_matrix,plot_pr_curve,save_dict_as_csv,plot_roc_curve,save_list_dict_as_csv
import matplotlib.pyplot as plt

if __name__ == '__main__':
    path_roc_csv = basePath() + "/data/roc.csv"
    data = pd.read_csv(path_roc_csv)

    #print([x for x in range(0,2740)])


    rf_y_test = data.iloc[0].values
    rf_y_pred = data.iloc[1].values
    plot_roc_curve(rf_y_test, rf_y_pred,'RF')

    mlp_y_test = data.iloc[2, :].values
    mlp_y_pred = data.iloc[3, :].values
    plot_roc_curve(mlp_y_test, mlp_y_pred, 'MLP')

    cnn_y_test = data.iloc[4, :].values
    cnn_y_pred = data.iloc[5, :].values
    plot_roc_curve(cnn_y_test, cnn_y_pred, 'CNN')

    plt.show()

